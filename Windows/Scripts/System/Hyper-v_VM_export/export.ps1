$VmList = Get-VM | Select Name
$exportPath = "D:\Virtualisation\Hyper-V\Export"
$start = $true

while ($start) {
    Write-Host "Available VM :"
    Foreach($vm in $VmList) 
    { 
        Write-Host $vm.Name
    }
    $VmName = Read-Host -Prompt 'Input your VM  name enter "A" to select (All VM)'

    if ( $VmName -eq "A"-or $VmName -eq "a" )
    {
        Foreach($vm in $VmList) 
        { 
            $VmName =  $vm.Name
            Export-VM -Name $VmName -Path $exportPath -CaptureLiveState CaptureSavedState
        }

        $start = $false
    } else {
        $Exists = get-vm -name $VmName -ErrorAction SilentlyContinue
        If ($Exists){
            Export-VM -Name $VmName -Path $exportPath -CaptureLiveState CaptureSavedState
            $start = $false
        }
        Else {
            Write-Host "Error wrong VM Name"
            $start = $true
        }
    }
}




#$Source = "c:\BackupFrom\backMeUp.txt"
#      $Target = "c:\TEMP\backup.zip"




<# Pour la copie sur serveur FTP
$File = "D:\Dev\somefilename.zip";
$ftp = "ftp://username:password@example.com/pub/incoming/somefilename.zip";

Write-Host -Object "ftp url: $ftp";

$webclient = New-Object -TypeName System.Net.WebClient;
$uri = New-Object -TypeName System.Uri -ArgumentList $ftp;

Write-Host -Object "Uploading $File...";

$webclient.UploadFile($uri, $File);

#>


#Import-VM -Path "C:\TEMP\Windows-10\Virtual Machines\3ED89FF1-7926-449D-AF2C-17877160D5F5.vmcx" -Copy -GenerateNewId `
#     -VirtualMachinePath "C:\ProgramData\Microsoft\Windows\Hyper-V\Virtual Machines\Windows-10-Clone" `
#     -VhdDestinationPath "C:\ProgramData\Microsoft\Windows\Hyper-V\Virtual Machines\Windows-10-Clone"

#$VM = Get-VM | Where-Object {$_.Path.StartsWith("C:\ProgramData\Microsoft\Windows\Hyper-V\Virtual Machines\Windows-10-Clone")}
#Rename-VM -VM $VM -NewName Windows-10-Clone

#Suppression du clone de la VM
#Remove-Item -Path "C:\TEMP\Windows-10" -Recurse -Force