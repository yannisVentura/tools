# Scripting tools

This project contain some of my scripts for Windows and Linux ( Debian ) based os.

## Windows

### Activate Powershell script on your computer

To activate Powershell Script on you computer open Powershell as an administrator and enter the following command :

    `Set-ExecutionPolicy RemoteSigned`


## Linux

### Debian scripts

I principally use Debian based distributions like Ubuntu or Linux Mint so I decided to regroup somes scripts I currently used.

### Install Powershell on Linux 

To install powershell on Linux, please follow the Microsoft documentation :
https://docs.microsoft.com/fr-fr/powershell/scripting/install/installing-powershell-on-linux?view=powershell-7.2
