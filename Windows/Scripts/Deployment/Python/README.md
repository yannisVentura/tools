# README

pip_install_default.bat and pip_install_default.bat will install the following modules :
  
  > django : https://www.djangoproject.com/
  
  > autopep8 : https://pypi.org/project/autopep8/
  
  > pylint : https://pypi.org/project/pylint/
  
  > virtualenv : https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/
  
