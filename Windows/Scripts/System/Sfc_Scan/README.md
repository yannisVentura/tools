# SFC command

The SFC command is a Windows embeded verification tool to check Windows system files integrity.
All of thoses scripts must be launch in a administrator command line.

Two version of the script exists : a windows cmd command line based and a powershell based. Each of them has been seperate respectively in Powershell and Bat for respectively powershell and bat scripts.

scranfile.bat and scanfile.ps1 goals are to launch `sfcsfc /scanfile=<file>`. This command scann a file and repare it if necessary.

verifyonly.bat and verifyonly.ps1 goals are to launch `sfc /verifyonly`. This command scann Windows file systems ( without modifications or correction). It's goal is to detect errors.

scannow.bat and scannow.ps1 goals are to launch `sfc /scannow`. This command scann and repare Windows files systems if necessary.
