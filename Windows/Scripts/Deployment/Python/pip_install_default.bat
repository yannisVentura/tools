REM Install django, autopep8, pylint and virtualenv for the current user.

pip install --user --upgrade django
pip install --user --upgrade autopep8
pip install --user --upgrade pylint
pip install --user --upgrade virtualenv