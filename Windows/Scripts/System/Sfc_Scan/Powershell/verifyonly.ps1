# Using the sfc command with the /verifyonly option,
# System File Checker will scan all protected files and report any issues, but no changes are made.
sfc /verifyonly