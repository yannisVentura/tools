$InformationFilePath = "./dxdiagResult.txt"
$ipconfigFilePath = "./configIp.txt"
$diskinfoPath = "./diskInfo.txt"
$WindowsUpdateInfoPath = "./windowsUpdateInfo.txt"
$installedSoftwarePath = "./installedSoftware.txt"
$pnpDeviceInformations = "./PeriphInfos.txt"

#generate system informations from dx diag
dxdiag /dontskip /whql:off /64bit /t $InformationFilePath


$ipconfig = ipconfig /all
Set-Content -Value $ipconfig -Path $ipconfigFilePath

#Get disk info
$diskInfo = wmic diskdrive get Name,Model,Manufacturer,SerialNumber,FirmwareRevision,Size,Partitions,Status,Signature
Set-Content -Value $diskInfo -Path $diskInfoPath

#Get last windows update
Get-HotFix >> $WindowsUpdateInfoPath

#Get list of installed software
Get-ItemProperty HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\* | Select-Object DisplayName, DisplayVersion, Publisher, InstallDate | Format-Table -AutoSize > $installedSoftwarePath

#Get list of peripherics
$periphInfo = Get-PnpDevice -PresentOnly
Set-Content -Value $periphInfo -Path $pnpDeviceInformations